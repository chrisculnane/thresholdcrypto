# README #

This repository is intended to provide a framework for developing and understanding threshold cryptographic protocols. It contains an abstract communication and storage framework, as well as a protocol framework, to allow new and existing protocols to be rapidly prototyped, without needing to spend time implementing communication and storage classes. It is still very much a work in progress, and as such should not be used for production systems. 

### How do I get set up? ###
It is currently setup as an Eclipse project, in time it will be updated to use a more standard build system. As such, currently check it out as an Eclipse project, adds the dependencies in the libs directory to the build path and run one of the example ProtocolRunner classes. 

### Contribution guidelines ###
Contributions are very welcome. There are currently no JUnit tests and further refactoring required of the code. Additionally the protocols need reviewing to ensure compliance with the relevant academic papers. Please note, the current implementation of distributed threshold key generation uses the Feldman approach, not Pedersen or Gennaro. Caution should be applied when using Feldman, because there are attacks that can impact on key distribution. However, in certain circumstances it is acceptable - for example, where the presence of misbehaviour during key generation would result in the key being discarded and the dishonest party being permanently excluded. Longer term, it would be good to implement both Pedersen and Gennaro to provide a nice comparison.

### Documentation ###
A publicly readable PDF document is being compiled describing the different protocols, it is available on [ShareLatex](https://www.sharelatex.com/project/578db1b9ff27fa4474fd4461)

There is a further document detailing the Communication Layer Framework [here](https://www.sharelatex.com/project/578f254c37108bf42e40617d)