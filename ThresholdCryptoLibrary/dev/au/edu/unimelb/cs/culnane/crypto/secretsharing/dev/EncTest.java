/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.secretsharing.dev;

import java.math.BigInteger;
import java.security.SecureRandom;

import au.edu.unimelb.cs.culnane.crypto.P_Length;
import au.edu.unimelb.cs.culnane.crypto.ZpStarSafePrimeGroupRFC3526;
import au.edu.unimelb.cs.culnane.crypto.exceptions.GroupException;

public class EncTest {

	public static void main(String[] args) throws GroupException {

		ZpStarSafePrimeGroupRFC3526 group = new ZpStarSafePrimeGroupRFC3526();
		group.initialise(P_Length.P2046);
		BigInteger x =group.getRandomValueInQ(new SecureRandom());
		BigInteger y = group.getRandomValueInQ(new SecureRandom());
		BigInteger h = group.get_g().modPow(x, group.get_p());
		BigInteger c1 = group.get_g().modPow(y,group.get_p());
		BigInteger sk = h.modPow(y, group.get_p());
		BigInteger m = BigInteger.valueOf(12345678);
		BigInteger c2 = m.multiply(sk).mod(group.get_p());
		
		BigInteger mult = BigInteger.valueOf(12);
		c1=c1.multiply(mult).mod(group.get_p());
		c2=c2.multiply(mult).mod(group.get_p());
		
		BigInteger decKey =c1.modPow(x, group.get_p());
		BigInteger res  = c2.multiply(decKey.modInverse(group.get_p())).mod(group.get_p());
		
		
		System.out.println(res);
	}

}
