/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.dev;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import au.edu.unimelb.cs.culnane.crypto.bls.BLSKeyPair;
import au.edu.unimelb.cs.culnane.crypto.bls.BLSPrivateKey;
import au.edu.unimelb.cs.culnane.crypto.bls.BLSPublicKey;
import au.edu.unimelb.cs.culnane.crypto.bls.BLSSignature;
import au.edu.unimelb.cs.culnane.crypto.bls.BLSSystemParameters;
import au.edu.unimelb.cs.culnane.crypto.exceptions.BLSSignatureException;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.SecretShare;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.ShamirSecretSharing;
import it.unisa.dia.gas.jpbc.Element;

public class BLSSigTest2 {

	public BLSSigTest2() {
	}

	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, BLSSignatureException {
		BLSSystemParameters params = BLSSystemParameters.generateNewParameters("./pbc_params/d159.properties");
		BLSKeyPair[] kps = new BLSKeyPair[3];
		Element[] combinedPrivate = new Element[3];
		Element combinedPublic = null;
		for(int i=0;i<3;i++){
			kps[i] = BLSKeyPair.generateKeyPair(params);
			ShamirSecretSharing secretShare = new ShamirSecretSharing(3,2,params.getPairing().getG1().getOrder(),new SecureRandom());
			BigInteger[] coefs = secretShare.generateCoefficients(kps[i].getPrivateKey().getKey().toBigInteger());
			SecretShare[] shares =secretShare.generateShares(coefs);
			for(int j=0;j<3;j++){
				Element tempPrivate = params.getPairing().getZr().newElement(shares[j].getShare());
				
				if(combinedPrivate[j]==null){
					combinedPrivate[j] = tempPrivate;
				}else{
					combinedPrivate[j] = combinedPrivate[j].getImmutable().add(tempPrivate);
				}
			}
			if(combinedPublic==null){
				combinedPublic =kps[i].getPublicKey().getPublicKeyElement();
			}else{
				combinedPublic = combinedPublic.getImmutable().mul(kps[i].getPublicKey().getPublicKeyElement());
			}
			
		}
		
		
		
		BLSSignature sig = new BLSSignature("SHA1", params);
		Element[] sigs = new Element[3];
		for (int i = 0; i < 3; i++) {
			sig.initSign(new BLSPrivateKey(combinedPrivate[i]));
			sig.update("hello");
			sigs[i] = sig.sign();
		}

		sig.initVerify(new BLSPublicKey(combinedPublic));
		sig.update("hello");

		Element combined = null;
		ShamirSecretSharing secretSharing = new ShamirSecretSharing(3, 2, params.getPairing().getZr().getOrder(),
				new SecureRandom());
		BigInteger[] lagrangeWeights = secretSharing.computeLagrangeWeights(new boolean[] { true, true, true });

		for (int i = 0; i < sigs.length; i++) {
			if (combined == null) {
				if (sigs[i] != null) {
					combined = sigs[i].mul(lagrangeWeights[i]);
				}
			} else {
				combined = combined.duplicate().mul(sigs[i].mul(lagrangeWeights[i]));
			}

		}
		System.out.println(sig.verify(combined, false));
	}

}
