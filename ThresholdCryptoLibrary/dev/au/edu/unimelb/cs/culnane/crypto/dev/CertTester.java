/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.dev;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

import au.edu.unimelb.cs.culnane.crypto.catools.CertTools;
import au.edu.unimelb.cs.culnane.crypto.catools.DevCA;

public class CertTester {

	public static void main(String[] args) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, OperatorCreationException, IOException, InvalidKeyException, SignatureException, javax.security.cert.CertificateException, UnrecoverableKeyException {
		Security.addProvider(new BouncyCastleProvider());
		DevCA devCA = DevCA.getInstance();
		PKCS10CertificationRequest certReq = CertTools.generateKeyAndCSR("TestDeviceSSL", "./newtest.jks", devCA.keyCACertificate(),"SHA256withRSA",false);
		KeyStore ks = CertTools.loadKeyStore("./newtest.jks");
		Certificate cert = devCA.sign(certReq);
		ks.setKeyEntry("TestDeviceSSL", ks.getKey("TestDeviceSSL", "".toCharArray()),"".toCharArray(), new Certificate[]{cert,devCA.keyCACertificate()});
		ks.setCertificateEntry("TestDeviceSSLOne", cert);
		ks.store(new FileOutputStream("./newtest.jks"), "".toCharArray());
		//ks.setCertificateEntry("TestDevice", devCA.sign(certReq));
		
	}

}
