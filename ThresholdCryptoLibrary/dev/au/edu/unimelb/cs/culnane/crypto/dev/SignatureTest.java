/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.dev;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.PKIXParameters;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import au.edu.unimelb.cs.culnane.crypto.catools.CertTools;

public class SignatureTest {

	public static void main(String[] args)
			throws NoSuchAlgorithmException, NoSuchProviderException, CertificateException, FileNotFoundException,
			KeyStoreException, IOException, SignatureException, InvalidAlgorithmParameterException, InvalidKeyException,
			UnrecoverableKeyException, CertPathValidatorException, CertPathBuilderException {

		Security.addProvider(new BouncyCastleProvider());
		KeyStore peer1ks = CertTools.loadKeyStore("./Peer1.jks");
		Signature signer = Signature.getInstance("SHA256withRSA", "BC");
		signer.initSign((PrivateKey) peer1ks.getKey("Peer1", "".toCharArray()));

		signer.update("hello".getBytes());
		byte[] sig = signer.sign();
		KeyStore peer2ks = CertTools.loadKeyStore("./Peer2.jks");
		Signature verify = Signature.getInstance("SHA256withRSA", "BC");

		CertificateFactory cf = CertificateFactory.getInstance("X.509");

		List<Certificate> certs = new ArrayList<Certificate>(Arrays.asList(peer1ks.getCertificateChain("Peer1")));

		CertPath cp = cf.generateCertPath(certs);
		PKIXParameters pkixp = new PKIXParameters(peer2ks);
		pkixp.setRevocationEnabled(false);
		CertPathValidator cpv = CertPathValidator.getInstance("PKIX");
		
		X509CertSelector targetConstraints = new X509CertSelector();
		System.out.println(((X509Certificate) certs.get(0)).getSubjectX500Principal());
		targetConstraints.setSubject(((X509Certificate) certs.get(0)).getSubjectX500Principal());
		targetConstraints.setIssuer(((X509Certificate)peer2ks.getCertificate("DevCA")).getSubjectX500Principal());
		pkixp.setTargetCertConstraints(targetConstraints);
		PKIXCertPathValidatorResult pcpvr = (PKIXCertPathValidatorResult) cpv.validate(cp, pkixp);
		verify.initVerify(pcpvr.getPublicKey());
		verify.update("hello".getBytes());
		System.out.println(verify.verify(sig));

		
		/**X509CertSelector targetConstraints = new X509CertSelector();

		targetConstraints.setIssuer(((X509Certificate) peer1ks.getCertificate("Peer1")).getIssuerX500Principal());
		targetConstraints.setSerialNumber(((X509Certificate) peer1ks.getCertificate("Peer1")).getSerialNumber());

		PKIXBuilderParameters params = new PKIXBuilderParameters(peer2ks, targetConstraints);

		// CertStore store = new JcaCertStoreBuilder().setProvider("BC").addCertificates().build();

		
		// params.addCertStore(store);
		params.setRevocationEnabled(false); 

		CertPathBuilder pathBuilder = CertPathBuilder.getInstance("PKIX", "BC");
		PKIXCertPathBuilderResult res = (PKIXCertPathBuilderResult) pathBuilder.build(params);
		X509Certificate cert = (X509Certificate) res.getCertPath().getCertificates().get(0);
		
		*/
		/**CertPath cp = cf.generateCertPath(certs);
		PKIXParameters pkixp = new PKIXParameters(peer2ks);
		pkixp.setRevocationEnabled(false);

		CertPathValidator cpv = CertPathValidator.getInstance("PKIX");
		PKIXCertPathValidatorResult pcpvr = (PKIXCertPathValidatorResult) cpv.validate(cp, pkixp);

		System.out.println(pcpvr.toString());*/

		// return signer.sign();
	}

}
