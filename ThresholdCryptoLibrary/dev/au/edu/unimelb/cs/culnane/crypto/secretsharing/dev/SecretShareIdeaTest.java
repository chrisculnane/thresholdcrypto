/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.secretsharing.dev;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;

import au.edu.unimelb.cs.culnane.crypto.P_Length;
import au.edu.unimelb.cs.culnane.crypto.ZpStarSafePrimeGroupRFC3526;
import au.edu.unimelb.cs.culnane.crypto.exceptions.GroupException;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.SecretShare;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.ShamirSecretSharing;

public class SecretShareIdeaTest {

	public static void main(String[] args) throws GroupException {
		ZpStarSafePrimeGroupRFC3526 group = new ZpStarSafePrimeGroupRFC3526();
		group.initialise(P_Length.P2046);
		ShamirSecretSharing secretSharing = new ShamirSecretSharing(3, 3, group, new SecureRandom());
		BigInteger secret = BigInteger.valueOf(12345678);
		BigInteger[] coefs = secretSharing.generateCoefficients(secret);
		SecretShare[] shares = secretSharing.generateShares(coefs);
		
		BigInteger[] shareBigInts = new BigInteger[shares.length];
		int i=0;
		for(SecretShare s: shares){
			shareBigInts[i] = s.getShare();
			i++;
		}
		System.out.println("Test Secret Sharing" +  secretSharing.interpolate(shareBigInts));
		
		System.out.println(Arrays.toString(shareBigInts));
		BigInteger x =group.getRandomValueInQ(new SecureRandom());
		BigInteger h = group.get_g().modPow(x, group.get_p());
		
		BigInteger[][] encryptedShares = new BigInteger[shares.length][2];
		i=0;
		SecureRandom rand = new SecureRandom();
		BigInteger y = group.getRandomValueInQ(rand);
		for(BigInteger bs : shareBigInts){
			BigInteger c1 = group.get_g().modPow(y,group.get_p());
			BigInteger sk = h.modPow(y, group.get_p());
			BigInteger m = bs;
			BigInteger c2 = m.multiply(sk).mod(group.get_p());
			encryptedShares[i][0]=c1;
			encryptedShares[i][1]=c2;
			i++;
		}
		BigInteger[] interpVals = new BigInteger[shares.length];
		
		i=0;
		for(SecretShare s: shares){
			interpVals[i] = encryptedShares[i][1];
			i++;
		}
		
		BigInteger interpolated = secretSharing.interpolate(interpVals);
		i=0;
		BigInteger combinedGr = BigInteger.ONE;
		BigInteger[] indDecs = new BigInteger[shares.length];
		BigInteger[] lagrange = secretSharing.computeLagrangeWeights(new boolean[]{true,true,true});
		for(SecretShare s: shares){			
			combinedGr = combinedGr.multiply(encryptedShares[i][0]).mod(group.get_p());
			BigInteger decKey =encryptedShares[i][0].multiply(lagrange[i]).mod(group.get_q()).modPow(x, group.get_p());
			indDecs[i] = encryptedShares[i][1].multiply(lagrange[i]).mod(group.get_q()).multiply(decKey.modInverse(group.get_p())).mod(group.get_p());
			
			i++;
		}
		BigInteger res = BigInteger.ZERO;
		i=0;
		for(SecretShare s: shares){
			res = res.add(indDecs[i]).mod(group.get_q());
			
			i++;
		}
		System.out.println(res);
		System.out.println(Arrays.toString(indDecs));
		BigInteger combineddecKey =combinedGr.modPow(x, group.get_p());
		BigInteger intepDec = interpolated.multiply(combineddecKey.modInverse(group.get_p())).mod(group.get_p());
		System.out.println("Interpolated Dec" + intepDec );
		
		
		BigInteger decKey =encryptedShares[0][0].modPow(x, group.get_p());
		interpolated = interpolated.multiply(decKey.modInverse(group.get_p())).mod(group.get_p());
		//interpolated = interpolated.multiply(decKey.modInverse(group.get_p())).mod(group.get_p());
		
		System.out.println("Interpolated" + interpolated );
		
	}

}
