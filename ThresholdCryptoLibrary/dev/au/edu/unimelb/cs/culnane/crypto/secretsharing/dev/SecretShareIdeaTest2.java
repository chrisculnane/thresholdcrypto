/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.secretsharing.dev;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;

import au.edu.unimelb.cs.culnane.crypto.P_Length;
import au.edu.unimelb.cs.culnane.crypto.ZpStarSafePrimeGroupRFC3526;
import au.edu.unimelb.cs.culnane.crypto.exceptions.GroupException;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.SecretShare;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.ShamirSecretSharing;

public class SecretShareIdeaTest2 {

	public static void main(String[] args) throws GroupException {

		ZpStarSafePrimeGroupRFC3526 group = new ZpStarSafePrimeGroupRFC3526();
		group.initialise(P_Length.P2046);
		ShamirSecretSharing secretSharing = new ShamirSecretSharing(3, 3, group, new SecureRandom());
		BigInteger secret = BigInteger.valueOf(12345678);
		BigInteger[] coefs = secretSharing.generateCoefficients(secret);
		SecretShare[] shares = secretSharing.generateShares(coefs);

		BigInteger[] shareBigInts = new BigInteger[shares.length];
		int i = 0;
		for (SecretShare s : shares) {
			shareBigInts[i] = s.getShare();
			i++;
		}
		System.out.println("Test Secret Sharing" + secretSharing.interpolate(shareBigInts));
		BigInteger x = group.getRandomValueInQ(new SecureRandom());
		BigInteger h = group.get_g().modPow(x, group.get_p());
		if (true) {
			SecureRandom rand = new SecureRandom();
			BigInteger y = group.getRandomValueInQ(rand);
			BigInteger c1 = group.get_g().modPow(y, group.get_p());
			BigInteger sk = h.modPow(y, group.get_p());
			BigInteger m = group.get_g().modPow(secret, group.get_p());
			BigInteger c2 = m.multiply(sk).mod(group.get_p());

			c1 = c1.multiply(BigInteger.valueOf(2)).mod(group.get_p());
			c2 = c2.multiply(BigInteger.valueOf(2)).mod(group.get_p());
			/**
			 * BigInteger c1prime = c1.modPow(BigInteger.valueOf(10),
			 * group.get_p()); BigInteger c2prime =
			 * c2.modPow(BigInteger.valueOf(10), group.get_p()); BigInteger
			 * c1mult = BigInteger.ONE; BigInteger c2mult = BigInteger.ONE; for
			 * (int j = 0; j < 10; j++) { c1mult =
			 * c1mult.multiply(c1).mod(group.get_p()); c2mult =
			 * c2mult.multiply(c2).mod(group.get_p()); } //
			 * System.out.println("a:" + c1prime); // System.out.println("b:" +
			 * c1mult);
			 * 
			 * BigInteger decKey = c1prime.modPow(x, group.get_p()); BigInteger
			 * dec =
			 * c2prime.multiply(decKey.modInverse(group.get_p())).mod(group.
			 * get_p()); System.out.println(dec); System.out.println(
			 * group.get_g().modPow(secret.multiply(BigInteger.valueOf(10)),
			 * group.get_p()).mod(group.get_p()));
			 */
			BigInteger decKey = c1.modPow(x, group.get_p());
			BigInteger dec = c2.multiply(decKey.modInverse(group.get_p())).mod(group.get_p());
			System.out.println("T" + dec);
			System.out.println("T"+
					group.get_g().modPow(secret.multiply(BigInteger.valueOf(10)), group.get_p()).mod(group.get_p()));

		}

		BigInteger[][] encryptedShares = new BigInteger[shares.length][2];
		i = 0;

		for (BigInteger bigIntShare : shareBigInts) {
			SecureRandom rand = new SecureRandom();
			BigInteger y = group.getRandomValueInQ(rand);
			BigInteger c1 = group.get_g().modPow(y, group.get_p());
			BigInteger sk = h.modPow(y, group.get_p());
			BigInteger m = group.get_g().modPow(bigIntShare, group.get_p());
			BigInteger c2 = m.multiply(sk).mod(group.get_p());
			encryptedShares[i][0] = c1;
			encryptedShares[i][1] = c2;
			i++;
		}
		BigInteger[] lagrange = secretSharing.computeLagrangeWeights(new boolean[] { true, true, true });
		BigInteger t = BigInteger.valueOf(10);
		// System.out.println(encryptedShares[0][0].modPow(t, group.get_p()));
		for (i = 0; i < encryptedShares.length; i++) {
			// BigInteger count = BigInteger.ZERO;
			// for(int j=0;j<10;j++){
			// count = count.add(BigInteger.ONE);
			encryptedShares[i][0] = encryptedShares[i][0].modPow(lagrange[i], group.get_p());
			encryptedShares[i][1] = encryptedShares[i][1].modPow(lagrange[i], group.get_p());

		}
		BigInteger decKey1 = encryptedShares[0][0].modPow(x, group.get_p());
		BigInteger decrypted1 = encryptedShares[0][1].multiply(decKey1.modInverse(group.get_p())).mod(group.get_p());
		System.out.println("Dec:" + decrypted1);
		System.out.println("Orig:" + group.get_g().modPow(shareBigInts[0].multiply(lagrange[0]), group.get_p()));
		BigInteger decKey2 = encryptedShares[1][0].modPow(x, group.get_p());
		BigInteger decrypted2 = encryptedShares[1][1].multiply(decKey2.modInverse(group.get_p())).mod(group.get_p());
		System.out.println("Dec:" + decrypted2);
		System.out.println("Orig:" + group.get_g().modPow(shareBigInts[1].multiply(lagrange[1]), group.get_p()));
		BigInteger decKey3 = encryptedShares[2][0].modPow(x, group.get_p());
		BigInteger decrypted3 = encryptedShares[2][1].multiply(decKey3.modInverse(group.get_p())).mod(group.get_p());
		System.out.println("Dec:" + decrypted3);
		System.out.println("Orig:" + group.get_g().modPow(shareBigInts[2].multiply(lagrange[2]), group.get_p()));
		BigInteger ss = BigInteger.ZERO;
		for (i = 0; i < shares.length; i++) {
			ss = ss.add(shareBigInts[i].multiply(lagrange[i])).mod(group.get_p());
		}
		System.out.println("val" + ss);
		BigInteger combinedc1 = BigInteger.ONE;
		BigInteger combinedc2 = BigInteger.ONE;
		for (i = 0; i < encryptedShares.length; i++) {
			combinedc1 = combinedc1.multiply(encryptedShares[i][0]).mod(group.get_p());
			combinedc2 = combinedc2.multiply(encryptedShares[i][1]).mod(group.get_p());
		}
		// }
		BigInteger decKey = combinedc1.modPow(x, group.get_p());
		BigInteger decrypted = combinedc2.multiply(decKey.modInverse(group.get_p())).mod(group.get_p());
		System.out.println("Dec:" + decrypted);

		System.out.println("Orig:" + group.get_g().modPow(secret, group.get_p()));
		System.out.println("OrigCheck:" + group.get_g().modPow(ss, group.get_p()));

	}

}
