/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.dev;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;

import au.edu.unimelb.cs.culnane.crypto.bls.BLSKeyPair;
import au.edu.unimelb.cs.culnane.crypto.bls.BLSSystemParameters;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.SecretShare;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.ShamirSecretSharing;

public class BLSKeyShareTest {

	public BLSKeyShareTest() {
	}

	public static void main(String[] args) throws IOException {

		//Pairing pairing = PairingFactory.getPairing("au/edu/unimelb/cs/culnane/crypto/bls/params/d159.properties");
		BLSSystemParameters params = BLSSystemParameters.generateNewParameters("./pbc_params/d159.properties");
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair(params);
		System.out.println(keyPair.getPrivateKey().getKey().toBigInteger());
		ShamirSecretSharing secretShare = new ShamirSecretSharing(3,2,params.getPairing().getG1().getOrder(),new SecureRandom());
		BigInteger[] coefs = secretShare.generateCoefficients(keyPair.getPrivateKey().getKey().toBigInteger());
		SecretShare[] shares =secretShare.generateShares(coefs);
		BigInteger[] available = new BigInteger[shares.length];
		for(int i=0;i<available.length-1;i++){
			available[i]=shares[i].getShare();
		}
		System.out.println(secretShare.interpolate(available));
	}

}
