/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.secretsharing.dev;

import java.math.BigInteger;
import java.security.SecureRandom;

import au.edu.unimelb.cs.culnane.crypto.P_Length;
import au.edu.unimelb.cs.culnane.crypto.ZpStarSafePrimeGroupRFC3526;
import au.edu.unimelb.cs.culnane.crypto.exceptions.GroupException;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.SecretShare;

public class SecretShareIdeaTest5 {

	public static void main(String[] args) throws GroupException {

		ZpStarSafePrimeGroupRFC3526 group = new ZpStarSafePrimeGroupRFC3526();
		group.initialise(P_Length.P2046);
		ShamirSecretSharingTest secretSharing = new ShamirSecretSharingTest(3, 3, group, new SecureRandom());
		BigInteger secret = BigInteger.valueOf(12345678);
		BigInteger[] coefs = secretSharing.generateCoefficients(secret);
		SecretShare[] shares = secretSharing.generateShares(coefs);

		BigInteger[] shareBigInts = new BigInteger[shares.length];
		int i = 0;
		for (SecretShare s : shares) {
			shareBigInts[i] = s.getShare();
			i++;
		}
		System.out.println("Test Secret Sharing" + secretSharing.interpolate(shareBigInts));
		shareBigInts[2] =shareBigInts[2].add(BigInteger.valueOf(1));
		System.out.println("Test Secret Sharing" + secretSharing.interpolate(shareBigInts));
		
		
		
		BigInteger x = group.getRandomValueInQ(new SecureRandom());
		BigInteger h = group.get_g().modPow(x, group.get_p());
		
		BigInteger[][] encryptedShares = new BigInteger[shares.length][2];
		i = 0;

		for (BigInteger bigIntShare : shareBigInts) {
			SecureRandom rand = new SecureRandom();
			BigInteger y = group.getRandomValueInQ(rand);
			BigInteger c1 = group.get_g().modPow(y, group.get_p());
			BigInteger sk = h.modPow(y, group.get_p());
			BigInteger m = group.get_g().modPow(bigIntShare, group.get_p());
			BigInteger c2 = m.multiply(sk).mod(group.get_p());
			encryptedShares[i][0] = c1;
			encryptedShares[i][1] = c2;
			i++;
		}
		SecureRandom rand = new SecureRandom();
		BigInteger y = group.getRandomValueInQ(rand);
		BigInteger c1 = group.get_g().modPow(y, group.get_p());
		BigInteger sk = h.modPow(y, group.get_p());
		BigInteger m = group.get_g().modPow(BigInteger.ONE.negate(), group.get_p());
		BigInteger c2 = m.multiply(sk).mod(group.get_p());
		encryptedShares[0][0] = encryptedShares[0][0].multiply(c1).mod(group.get_p());
		encryptedShares[0][1] = encryptedShares[0][1].multiply(c2).mod(group.get_p());
		
		BigInteger[] lagrange = secretSharing.computeLagrangeWeights(new boolean[] { true, true, true });
		for (i = 0; i < encryptedShares.length; i++) {
			encryptedShares[i][0] = encryptedShares[i][0].modPow(lagrange[i], group.get_p());
			encryptedShares[i][1] = encryptedShares[i][1].modPow(lagrange[i], group.get_p());
		}
		BigInteger combinedc1 = BigInteger.ONE;
		BigInteger combinedc2 = BigInteger.ONE;
		for (i = 0; i < encryptedShares.length; i++) {
			combinedc1 = combinedc1.multiply(encryptedShares[i][0]).mod(group.get_p());
			combinedc2 = combinedc2.multiply(encryptedShares[i][1]).mod(group.get_p());
		}
		BigInteger decKey = combinedc1.modPow(x, group.get_p());
		BigInteger decrypted = combinedc2.multiply(decKey.modInverse(group.get_p())).mod(group.get_p());
		System.out.println("Dec:" + decrypted);
		System.out.println("Orig:" + group.get_g().modPow(secret.subtract(BigInteger.valueOf(3)), group.get_p()));


	}

}
