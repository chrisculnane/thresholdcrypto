/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.dev;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.json.JSONObject;

import au.edu.unimelb.cs.culnane.crypto.bls.BLSPrivateKey;
import au.edu.unimelb.cs.culnane.crypto.bls.BLSPublicKey;
import au.edu.unimelb.cs.culnane.crypto.bls.BLSSignature;
import au.edu.unimelb.cs.culnane.crypto.bls.BLSSystemParameters;
import au.edu.unimelb.cs.culnane.crypto.exceptions.BLSSignatureException;
import au.edu.unimelb.cs.culnane.crypto.secretsharing.ShamirSecretSharing;
import au.edu.unimelb.cs.culnane.storage.json.JSONStorageObject;
import au.edu.unimelb.cs.culnane.utils.IOUtils;
import au.edu.unimelb.cs.culnane.utils.IOUtils.EncodingType;
import it.unisa.dia.gas.jpbc.Element;

public class BLSSigTest {

	public BLSSigTest() {

	}

	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, BLSSignatureException {
		//BLSSystemParameters params = BLSSystemParameters.generateNewParameters("./pbc_params/d159.properties");
		BLSSystemParameters params = new BLSSystemParameters("params.json");
		BLSPublicKey[] blsKeys = new BLSPublicKey[3];
		BLSPrivateKey[] blsPrivKeys = new BLSPrivateKey[3];
		BLSSignature sig = new BLSSignature("SHA1", params);
		Element[] sigs = new Element[3];
		for (int i = 0; i < blsKeys.length; i++) {
			JSONObject key = IOUtils.readJSONObjectFromFile("peer" + (i+1) +".js");
			
			Element tempElem = params.getPairing().getG2().newElement();
			// Set the element value from the bytes decoded from the JSON
			//System.out.println(keyOne.toString());
			tempElem.setFromBytes(IOUtils.decodeData(EncodingType.BASE64,key.getString("h")));
			blsPrivKeys[i] = new BLSPrivateKey(new JSONStorageObject(key.getJSONObject("x")),params.getPairing());
			//tempElem2.setFromBytes(new BigInteger(key.getString("x"), 16).toByteArray());
			Element tempPartial = params.getPairing().getG2().newElement();
			// Set the element value from the bytes decoded from the JSON
			//System.out.println(keyOne.toString());
			tempPartial.setFromBytes(IOUtils.decodeData(EncodingType.BASE64,key.getJSONArray("verify").getString(i)));
			blsKeys[i] = new BLSPublicKey(tempElem,tempPartial);
			//blsPrivKeys[i] = new BLSPrivateKey(tempElem2);
			sig.initSign(blsPrivKeys[i]);
			sig.update("hello");
			sigs[i] = sig.sign();
		}
		sig.initVerify(blsKeys[0]);
		sig.update("hello");
		BLSSignature partialsig = new BLSSignature("SHA1", params);
		Element combined = null;
		ShamirSecretSharing secretSharing = new ShamirSecretSharing(3, 2, params.getPairing().getZr().getOrder(),
				new SecureRandom());
		BigInteger[] lagrangeWeights = secretSharing.computeLagrangeWeights(new boolean[] { true, true, true });

		for (int i = 0; i < sigs.length; i++) {
			//System.out.println(blsKeys[i].toJSON().toString(2));
			partialsig.initVerify(blsKeys[i]);
			partialsig.update("hello");
			
			System.out.println("Partial:"+partialsig.verify(sigs[i], true));
			if (combined == null) {
				if (sigs[i] != null) {
					combined = sigs[i].mul(lagrangeWeights[i]);
				}
			} else {
				combined = combined.duplicate().mul(sigs[i].mul(lagrangeWeights[i]));
			}

		}
		System.out.println(sig.verify(combined, false));
	}

}
