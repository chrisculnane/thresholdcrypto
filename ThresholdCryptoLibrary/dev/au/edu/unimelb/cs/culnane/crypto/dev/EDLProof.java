/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.crypto.dev;

import java.math.BigInteger;

/**
 * Class that contains a Equality of Discrete Log Zero Knowledge Proof
 * 
 * @author Chris Culnane
 *
 */
public class EDLProof {
	/**
	 * BigInteger containing g1
	 */
	public BigInteger g1;
	/**
	 * BigInteger containing g2
	 */
	public BigInteger g2;
	/**
	 * BigInteger containing y1
	 */
	public BigInteger y1;
	/**
	 * BigInteger containing y2
	 */
	public BigInteger y2;
	/**
	 * BigInteger containing c
	 */
	public BigInteger c;
	/**
	 * BigInteger containing b
	 */
	public BigInteger b;

	/**
	 * Constructs an EDLProof object from the specified BigInteger parameters
	 * 
	 * @param g1
	 *            BigInteger containing g1
	 * @param g2
	 *            BigInteger containing g2
	 * @param y1
	 *            BigInteger containing y1
	 * @param y2
	 *            BigInteger containing y2
	 * @param c
	 *            BigInteger containing c
	 * @param b
	 *            BigInteger containing b
	 */
	public EDLProof(BigInteger g1, BigInteger g2, BigInteger y1, BigInteger y2, BigInteger c, BigInteger b) {
		this.g1 = g1;
		this.g2 = g2;
		this.y1 = y1;
		this.y2 = y2;
		this.c = c;
		this.b = b;
	}
}