/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.comms.dev;

import java.io.IOException;

import au.edu.unimelb.cs.culnane.comms.CommunicationLayerMessageListener;
import au.edu.unimelb.cs.culnane.comms.exceptions.UnknownChannelIdException;
import au.edu.unimelb.cs.culnane.comms.json.JSONCommLayerMessage;
import au.edu.unimelb.cs.culnane.comms.json.JSONCommLayerMessageConstructor;
import au.edu.unimelb.cs.culnane.comms.layers.inmemory.InMemoryChannel;
import au.edu.unimelb.cs.culnane.comms.layers.inmemory.InMemoryLayer;

public class TestJSONComms  implements
CommunicationLayerMessageListener<JSONCommLayerMessage> {
	private String id;
	public TestJSONComms(String id) {
		this.id=id;

	}

	public static void main(String[] args) throws UnknownChannelIdException, IOException {
		
		InMemoryLayer<JSONCommLayerMessage> alice = new InMemoryLayer<JSONCommLayerMessage>(new JSONCommLayerMessageConstructor());
		alice.addMessageListener(new TestJSONComms("alice"));
		InMemoryLayer<JSONCommLayerMessage> bob = new InMemoryLayer<JSONCommLayerMessage>(new JSONCommLayerMessageConstructor());
		bob.addMessageListener(new TestJSONComms("bob"));
		InMemoryLayer<JSONCommLayerMessage> charlie = new InMemoryLayer<JSONCommLayerMessage>(new JSONCommLayerMessageConstructor());
		charlie.addMessageListener(new TestJSONComms("charlie"));
		alice.addChannel(new InMemoryChannel<JSONCommLayerMessage>("bob",bob));
		alice.addChannel(new InMemoryChannel<JSONCommLayerMessage>("charlie",charlie));
		bob.addChannel(new InMemoryChannel<JSONCommLayerMessage>("alice",alice));
		bob.addChannel(new InMemoryChannel<JSONCommLayerMessage>("charlie",charlie));
		charlie.addChannel(new InMemoryChannel<JSONCommLayerMessage>("alice",alice));
		charlie.addChannel(new InMemoryChannel<JSONCommLayerMessage>("bob",bob));
		
		JSONCommLayerMessage msg = new JSONCommLayerMessage();
		msg.put("hello", "test");
		//alice.send(msg, "bob");
		alice.broadcast(msg);
		
	}

	@Override
	public void messageReceived(JSONCommLayerMessage msg) {
		System.out.println(id + " received " + msg.toString());
		
	}

}
