/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.comms.dev;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.prng.FixedSecureRandom;
import org.bouncycastle.crypto.prng.SP800SecureRandom;
import org.bouncycastle.crypto.prng.SP800SecureRandomBuilder;

public class SecureRandomTest {

	public static void main(String[] args) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		
		// Use DRBG from BouncyCastle to prepare source of randomness from hash
		// output
		FixedSecureRandom fsr = new FixedSecureRandom(md.digest("hello this is a test".getBytes()));

		// We disable predictionResistant since this would require re-seeding
		// and we only have a finite amount of seed randomness
		SP800SecureRandomBuilder rBuild = new SP800SecureRandomBuilder(fsr, false);
		rBuild.setPersonalizationString("CoinFlip".getBytes());
		SP800SecureRandom drbg = rBuild.buildHash(new SHA256Digest(), null, false);
		System.out.println(drbg.nextInt());
		System.out.println(drbg.nextInt());
		System.out.println(drbg.nextInt());
		System.out.println(drbg.nextInt());
		System.out.println(drbg.nextInt());
		System.out.println(drbg.nextInt());
		System.out.println(drbg.nextInt());
		System.out.println(drbg.nextInt());
		System.out.println(drbg.nextInt());
		System.out.println(drbg.nextInt());
/**
 * 476941960
832055490
-206607040
1251497753
-1306863498
926831363
1929636531
1876472385
1328680235
1940874697

 */
	}

}
