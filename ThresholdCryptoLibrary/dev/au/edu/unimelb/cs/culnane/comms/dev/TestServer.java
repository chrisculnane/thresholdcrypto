/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.comms.dev;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.edu.unimelb.cs.culnane.comms.CommunicationLayerMessageListener;
import au.edu.unimelb.cs.culnane.comms.exceptions.UnknownChannelIdException;
import au.edu.unimelb.cs.culnane.comms.json.JSONCommLayerMessage;
import au.edu.unimelb.cs.culnane.comms.json.JSONCommLayerMessageConstructor;
import au.edu.unimelb.cs.culnane.comms.layers.socket.SocketLayer;
import au.edu.unimelb.cs.culnane.comms.layers.socket.SocketLayerChannel;

public class TestServer implements
		CommunicationLayerMessageListener<JSONCommLayerMessage> {
	private static final Logger logger = LoggerFactory
			.getLogger(TestServer.class);
	private SocketLayer<JSONCommLayerMessage> socketLayer;
	private String id;
	private int port;

	public TestServer(String id, int port) throws IOException {
		this.id = id;
		this.port = port;
		this.socketLayer = new SocketLayer<JSONCommLayerMessage>(
				new JSONCommLayerMessageConstructor(), port);
		socketLayer.addMessageListener(this);
		socketLayer.start();
	}

	public JSONObject getConnectionInfo() {
		JSONObject conn = new JSONObject();
		conn.put("id", this.id);
		conn.put("port", this.port);
		conn.put("host", "localhost");
		return conn;
	}

	public void createChannel(JSONObject conninfo) throws IOException {
		SocketLayerChannel<JSONCommLayerMessage> channel = new SocketLayerChannel<JSONCommLayerMessage>(
				conninfo.getString("id"), conninfo.getString("host"),
				conninfo.getInt("port"));
		channel.open();
		socketLayer.addChannel(channel);
	}

	public void connectToAll(JSONArray connectionInfo) throws JSONException,
			IOException {
		for (int i = 0; i < connectionInfo.length(); i++) {
			if (!connectionInfo.getJSONObject(i).getString("id")
					.equals(this.id)) {
				createChannel(connectionInfo.getJSONObject(i));
			}
		}
	}

	public void send(JSONCommLayerMessage msg, String channelId)
			throws UnknownChannelIdException, IOException {
		socketLayer.send(msg, channelId);
	}

	public void broadcast(JSONCommLayerMessage msg) throws UnknownChannelIdException,
			IOException {
		socketLayer.broadcast(msg);
	}

	@Override
	public void messageReceived(JSONCommLayerMessage msg) {
		logger.info("Server {} received {}", this.id, msg);
		JSONCommLayerMessage reply = new JSONCommLayerMessage();
		reply.put("message", "nice one alice");
		try {
			this.send(reply,"alice");
		} catch (UnknownChannelIdException | IOException e) {

			e.printStackTrace();
		}
	}

}
