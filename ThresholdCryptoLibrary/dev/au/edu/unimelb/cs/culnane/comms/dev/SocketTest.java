/*******************************************************************************
 * Copyright (c) 2016 University of Melbourne
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *    Chris Culnane - initial API and implementation
 *******************************************************************************/
package au.edu.unimelb.cs.culnane.comms.dev;

import java.io.IOException;

import org.json.JSONArray;

import au.edu.unimelb.cs.culnane.comms.CommunicationLayerMessageListener;
import au.edu.unimelb.cs.culnane.comms.exceptions.UnknownChannelIdException;
import au.edu.unimelb.cs.culnane.comms.json.JSONCommLayerMessage;

public class SocketTest implements CommunicationLayerMessageListener<JSONCommLayerMessage>{
	private String id;
	public SocketTest(String id) {
		this.id=id;

	}
	public static void main(String[] args) throws NumberFormatException, IOException, UnknownChannelIdException{
		
		TestServer alice = new TestServer("alice",9091);
		TestServer bob = new TestServer("bob",9092);
		TestServer charlie = new TestServer("charlie",9093);
		TestServer dave = new TestServer("dave",9094);
		JSONArray conns = new JSONArray();
		conns.put(alice.getConnectionInfo());
		conns.put(bob.getConnectionInfo());
		conns.put(charlie.getConnectionInfo());
		conns.put(dave.getConnectionInfo());
		
		alice.connectToAll(conns);
		bob.connectToAll(conns);
		charlie.connectToAll(conns);
		dave.connectToAll(conns);
		
		JSONCommLayerMessage test = new JSONCommLayerMessage();
		test.put("message", "ahoy hoy");
		alice.broadcast(test);
		
		
		
		
		
		
	}
	@Override
	public void messageReceived(JSONCommLayerMessage msg) {
		System.out.println(id + " received:" + msg.toString());
		
	}

}
