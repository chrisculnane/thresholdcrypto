package au.edu.unimelb.cs.culnane.threshold;

public interface ProtocolParam {
	public String encode();
	public String getName();
	public Object getValue();
	public void decode(String name, String value);
}
