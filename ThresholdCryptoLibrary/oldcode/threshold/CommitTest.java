package au.edu.unimelb.cs.culnane.threshold;

import java.math.BigInteger;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import au.edu.unimelb.cs.culnane.crypto.exceptions.GroupValidationException;

public class CommitTest {

	public CommitTest() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws JSONException, UnknownProtocolStepException, GroupValidationException, InvalidInitialisationException, InvalidCommitValueException {
		// TODO Auto-generated method stub
		PedersenCommitment bob = new PedersenCommitment();
		bob.initialise(1024);
		PedersenCommitment alice = new PedersenCommitment();
		JSONObject msg =bob.createMessage(PedersenCommitment.ProtocolSteps.INITIALISE);
		alice.receiveMessage(msg);
		BigInteger testValue = BigInteger.valueOf(123456);
		msg = alice.commit(testValue);
		bob.receiveMessage(msg);
		msg = alice.getOpening(UUID.fromString(msg.getString("commitId")), testValue);
		bob.receiveMessage(msg);
	}

}
