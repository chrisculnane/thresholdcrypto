package au.edu.unimelb.cs.culnane.threshold;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.edu.unimelb.cs.culnane.crypto.CryptoUtils;
import au.edu.unimelb.cs.culnane.crypto.ZpStarSafePrimeGroup;
import au.edu.unimelb.cs.culnane.crypto.exceptions.GroupValidationException;

public class PedersenCommitment {

	private static final Logger logger = LoggerFactory
			.getLogger(PedersenCommitment.class);
	private BigInteger v;
	private ZpStarSafePrimeGroup group;
	private static final String PROTOCOL = "PedersenCommitment";

	private HashMap<UUID,BigInteger> commits = new HashMap<UUID,BigInteger>();
	private enum InternalState {
		UNINITIALISED, INITIALISED_AS_RECEIVER, INITIALISED_AS_SENDER
	}

	private InternalState currentState = InternalState.UNINITIALISED;

	public enum ProtocolSteps {
		INITIALISE, COMMIT, OPEN
	}

	public PedersenCommitment() {

	}

	public void initialise(int q_length) {
		logger.info("Initalising group");
		SecureRandom rand = new SecureRandom();
		group = new ZpStarSafePrimeGroup();
		group.initialise(1024, 2, rand);
		BigInteger a = CryptoUtils.getRandomInteger(group.get_q(), rand);
		v = group.createGenerator(rand);
		this.currentState = InternalState.INITIALISED_AS_RECEIVER;
		// v = CryptoUtils.getRandomInteger(
		// group.get_q().subtract(BigInteger.ONE), rand, BigInteger.ONE);
		logger.info("Finished initalising group");
	}

	public JSONObject createMessage(ProtocolSteps protocolStep,ProtocolParam... params)
			throws UnknownProtocolStepException {
		JSONObject msg = new JSONObject();
		switch (protocolStep) {
		case INITIALISE:
			msg.put("protocol", PROTOCOL);
			msg.put("step", protocolStep.toString());
			msg.put("p", group.get_p().toString(16));
			msg.put("q", group.get_q().toString(16));
			msg.put("g", group.get_g().toString(16));
			msg.put("v", v.toString(16));
			break;
		case COMMIT:
		case OPEN:
			msg.put("protocol", PROTOCOL);
			msg.put("step", protocolStep.toString());
			for(ProtocolParam p : params){
				msg.put(p.getName(), p.encode());
			}
			break;
		default:
			throw new UnknownProtocolStepException(
					"The protocol step is undefined");

		}
		return msg;
	}

	public void receiveMessage(JSONObject msg)
			throws UnknownProtocolStepException, JSONException,
			GroupValidationException {
		logger.info("Received message:{}", msg);
		switch (ProtocolSteps.valueOf(msg.getString("step"))) {
		case INITIALISE:
			if (verifyAndInitConstruction(
					new BigInteger(msg.getString("p"), 16),
					new BigInteger(msg.getString("q"), 16),
					new BigInteger(msg.getString("g"), 16),
					new BigInteger(msg.getString("v"), 16))) {
				this.currentState = InternalState.INITIALISED_AS_SENDER;
				logger.info("Is valid");
			}
			break;
		case COMMIT:
			commits.put(UUID.fromString(msg.getString("commitId")),new BigInteger(msg.getString("c"),16));
			break;
		case OPEN:
			if(verifyCommitment(new BigInteger(msg.getString("r"),16),new BigInteger(msg.getString("m"),16),UUID.fromString(msg.getString("commitId")))){
				logger.info("Commit Verifies");
			}
			break;
		default:
			throw new UnknownProtocolStepException(
					"The protocol step is undefined");

		}
	}

	private boolean verifyAndInitConstruction(BigInteger p, BigInteger q,
			BigInteger g, BigInteger v) throws GroupValidationException {
		this.group = new ZpStarSafePrimeGroup(p, q, g);
		if (!group.verify_p_isPrime(2)) {
			logger.error("Invalid group, p is not prime");
			throw new GroupValidationException("p is not prime");
		}
		if (!group.verify_q_isPrime(2)) {
			logger.error("Invalid group, q is not prime");
			throw new GroupValidationException("q is not prime");
		}

		if (!group.verify_q_divides_p_minus_one()) {
			logger.error("Invalid group, q does not divide p-1");
			throw new GroupValidationException("q does not divide p-1");
		}
		if (!group.verify_g_order()) {
			logger.error("Invalid group, g is not order q");
			throw new GroupValidationException("g is not order q");
		}
		if (!group.verify_element_order(v)) {
			logger.error("Invalid group, v is not order q");
			throw new GroupValidationException("v is not order q");
		}
		this.v=v;
		return true;

	}

	private boolean verifyCommitment(BigInteger r, BigInteger m, UUID commitId){
		BigInteger commit = this.commits.get(commitId);
		BigInteger reconstructedCommit = group.get_g().modPow(r, group.get_p()).multiply(v.modPow(m, group.get_p()));
		return ((commit.equals(reconstructedCommit)));
	}
	public JSONObject commit(BigInteger m) throws InvalidInitialisationException, InvalidCommitValueException, UnknownProtocolStepException {
		// TODO Check we are in a state that allows commitment
		if(!(this.currentState==InternalState.INITIALISED_AS_SENDER)){
			throw new InvalidInitialisationException("Cannot send a commit when initialised as a receiver");
		}
		if(m.compareTo(BigInteger.ZERO)<0 || m.compareTo(group.get_q().subtract(BigInteger.ONE))>0){
			throw new InvalidCommitValueException("Commit value is either too small or too large for the group");
		}
		BigInteger r =CryptoUtils.getRandomInteger(group.get_q().subtract(BigInteger.ONE), new SecureRandom());
		UUID commitId = UUID.randomUUID();
		this.commits.put(commitId, r);
		BigInteger commit = group.get_g().modPow(r, group.get_p()).multiply(v.modPow(m, group.get_p()));
		return this.createMessage(ProtocolSteps.COMMIT,new BigIntegerParam("c",commit),new UUIDParam("commitId",commitId));
	}
	public JSONObject getOpening(UUID commitId, BigInteger m) throws UnknownProtocolStepException{
		return this.createMessage(ProtocolSteps.OPEN,new BigIntegerParam("r",this.commits.get(commitId)),new BigIntegerParam("m",m),new UUIDParam("commitId",commitId));
	}

}
