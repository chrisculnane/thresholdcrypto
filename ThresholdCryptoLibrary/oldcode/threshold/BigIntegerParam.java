package au.edu.unimelb.cs.culnane.threshold;

import java.math.BigInteger;

public class BigIntegerParam implements ProtocolParam {

	private String name;
	private BigInteger value;
	public BigIntegerParam(String name, BigInteger value) {
		this.name=name;
		this.value=value;
	}
	
	public BigIntegerParam(){
		
	}
	@Override
	public String encode() {
		return value.toString(16);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Object getValue() {
		return value;
	}
	public BigInteger value(){
		return this.value;
	}


	@Override
	public void decode(String name, String value) {
		this.name=name;
		this.value = new BigInteger(value,16);
	}

}
