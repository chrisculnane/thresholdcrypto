package au.edu.unimelb.cs.culnane.threshold;

import java.math.BigInteger;
import java.util.UUID;

public class UUIDParam implements ProtocolParam {

	private String name;
	private UUID value;
	public UUIDParam(String name, UUID value) {
		this.name=name;
		this.value=value;
	}
	
	public UUIDParam(){
		
	}
	@Override
	public String encode() {
		return value.toString();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Object getValue() {
		return value;
	}
	public UUID value(){
		return this.value;
	}


	@Override
	public void decode(String name, String value) {
		this.name=name;
		this.value =UUID.fromString(value);
	}

}
