package au.edu.unimelb.cs.culnane.threshold;

import java.math.BigInteger;
import java.util.HashMap;

public class ProtocolParams {

	private HashMap<String,BigInteger> params = new HashMap<String,BigInteger>();
	public ProtocolParams() {
		
	}
	public void setParam(String name, BigInteger value){
		this.params.put(name, value);
	}
	public BigInteger getParam(String name) throws ParameterNotFoundException{
		if(!this.params.containsKey(name)){
			throw new ParameterNotFoundException("Parameter " + name + " not found");
		}
		return this.params.get(name);
	}

}
