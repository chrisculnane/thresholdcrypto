package au.edu.unimelb.cs.culnane.comms.tests;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import org.bouncycastle.operator.OperatorCreationException;
import org.json.JSONArray;

import au.edu.unimelb.cs.culnane.comms.CommunicationLayerMessageListener;
import au.edu.unimelb.cs.culnane.comms.UnknownChannelIdException;
import au.edu.unimelb.cs.culnane.comms.json.JSONMessage;
import au.edu.unimelb.cs.culnane.comms.json.JSONMessageConstructor;
import au.edu.unimelb.cs.culnane.comms.layers.socket.SocketLayer;
import au.edu.unimelb.cs.culnane.comms.layers.socket.SocketLayerChannel;

public class SecureSocketTest implements CommunicationLayerMessageListener<JSONMessage>{
	private String id;
	public SecureSocketTest(String id) {
		this.id=id;
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) throws NumberFormatException, IOException, UnknownChannelIdException, KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, CertificateException, KeyStoreException, InvalidAlgorithmParameterException, InvalidKeyException, NoSuchProviderException, SignatureException, OperatorCreationException, javax.security.cert.CertificateException{
		//System.setProperty("javax.net.debug", "all");
		TestSSLServer alice = new TestSSLServer("alice",9091);
		TestSSLServer bob = new TestSSLServer("bob",9092);
		TestSSLServer charlie = new TestSSLServer("charlie",9093);
		TestSSLServer dave = new TestSSLServer("dave",9094);
		JSONArray conns = new JSONArray();
		conns.put(alice.getConnectionInfo());
		conns.put(bob.getConnectionInfo());
		conns.put(charlie.getConnectionInfo());
		conns.put(dave.getConnectionInfo());
		
		alice.connectToAll(conns);
		bob.connectToAll(conns);
		charlie.connectToAll(conns);
		dave.connectToAll(conns);
		
		JSONMessage test = new JSONMessage();
		test.put("message", "ahoy hoy");
		alice.broadcast(test);
		
		
		
		
		
		
	}
	@Override
	public void messageReceived(JSONMessage msg) {
		System.out.println(id + " received:" + msg.toString());
		
	}

}
