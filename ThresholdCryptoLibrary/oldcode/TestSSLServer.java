package au.edu.unimelb.cs.culnane.comms.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.X509CertSelector;
import java.util.Arrays;

import javax.net.ssl.CertPathTrustManagerParameters;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.edu.unimelb.cs.culnane.comms.CommunicationLayerMessageListener;
import au.edu.unimelb.cs.culnane.comms.UnknownChannelIdException;
import au.edu.unimelb.cs.culnane.comms.json.JSONMessage;
import au.edu.unimelb.cs.culnane.comms.json.JSONMessageConstructor;
import au.edu.unimelb.cs.culnane.comms.layers.securesocket.SocketLayer;
import au.edu.unimelb.cs.culnane.comms.layers.securesocket.SocketLayerChannel;
import au.edu.unimelb.cs.culnane.crypto.CertTools;
import au.edu.unimelb.cs.culnane.crypto.DevCA;


public class TestSSLServer implements
		CommunicationLayerMessageListener<JSONMessage> {
	private static final Logger logger = LoggerFactory
			.getLogger(TestSSLServer.class);
	private SocketLayer<JSONMessage> socketLayer;
	private String id;
	private int port;
	private SSLContext ctx;
	private TrustManagerFactory tmf;
	private KeyManagerFactory kmf;
	private SSLServerSocketFactory sslSocketFactory;
	

	public TestSSLServer(String id, int port) throws IOException, KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, CertificateException, KeyStoreException, InvalidAlgorithmParameterException, InvalidKeyException, NoSuchProviderException, SignatureException, OperatorCreationException, javax.security.cert.CertificateException {
		this.id = id;
		this.port = port;
		Security.addProvider(new BouncyCastleProvider());
		DevCA devCA = DevCA.getInstance();
		File ksFile = new File("./"+id+".jks");
		if(!ksFile.exists()){
			PKCS10CertificationRequest certReq = CertTools.generateKeyAndCSR(id, "./" + id + ".jks", devCA.keyCACertificate(),"SHA256withRSA",false);
			KeyStore ks = CertTools.loadKeyStore("./" + id + ".jks");
			Certificate cert = devCA.sign(certReq);
			ks.setKeyEntry(id, ks.getKey(id, "".toCharArray()),"".toCharArray(), new Certificate[]{cert,devCA.keyCACertificate()});
			
			ks.store(new FileOutputStream("./" + id + ".jks"), "".toCharArray());
		}
		
		initTruststore("./" + id + ".jks");
		
		this.ctx = SSLContext.getInstance("TLSv1.2");
		
		this.ctx.init(this.kmf.getKeyManagers(), this.tmf.getTrustManagers(), new SecureRandom());
		//logger.info("Cipher suites:{}", Arrays.toString(this.ctx.getSupportedSSLParameters().getCipherSuites()));
		this.sslSocketFactory = this.ctx.getServerSocketFactory();

		
		this.socketLayer = new SocketLayer<JSONMessage>(
				new JSONMessageConstructor(), port,this.sslSocketFactory);
		socketLayer.addMessageListener(this);
		socketLayer.start();
	}

	public void initTruststore(String pathToKS)
			throws NoSuchAlgorithmException, CertificateException, FileNotFoundException, KeyStoreException,
			IOException, InvalidAlgorithmParameterException, UnrecoverableKeyException {
		KeyStore ks = CertTools.loadKeyStore(pathToKS);

		this.tmf = TrustManagerFactory.getInstance("PKIX");
		PKIXBuilderParameters pkixParams = new PKIXBuilderParameters(ks, new X509CertSelector());
		pkixParams.setRevocationEnabled(false);
		//pkixParams.se
		tmf.init(ks);//new CertPathTrustManagerParameters(pkixParams));
		
		this.kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		this.kmf.init(ks, "".toCharArray());
		
	}

	
	public JSONObject getConnectionInfo() {
		JSONObject conn = new JSONObject();
		conn.put("id", this.id);
		conn.put("port", this.port);
		conn.put("host", "localhost");
		return conn;
	}

	public void createChannel(JSONObject conninfo) throws IOException {
		SocketLayerChannel<JSONMessage> channel = new SocketLayerChannel<JSONMessage>(
				conninfo.getString("id"), conninfo.getString("host"),
				conninfo.getInt("port"),this.ctx.getSocketFactory());
		channel.open();
		socketLayer.addChannel(channel);
	}

	public void connectToAll(JSONArray connectionInfo) throws JSONException,
			IOException {
		for (int i = 0; i < connectionInfo.length(); i++) {
			if (!connectionInfo.getJSONObject(i).getString("id")
					.equals(this.id)) {
				createChannel(connectionInfo.getJSONObject(i));
			}
		}
	}

	public void send(JSONMessage msg, String channelId)
			throws UnknownChannelIdException, IOException {
		socketLayer.send(msg, channelId);
	}

	public void broadcast(JSONMessage msg) throws UnknownChannelIdException,
			IOException {
		socketLayer.broadcast(msg);
	}

	@Override
	public void messageReceived(JSONMessage msg) {
		logger.info("Server {} received {}", this.id, msg);
		JSONMessage reply = new JSONMessage();
		reply.put("message", "nice one alice");
		try {
			this.send(reply,"alice");
		} catch (UnknownChannelIdException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
