package au.edu.unimelb.cs.culnane.comms.layers.securesocket;

import java.util.concurrent.LinkedBlockingQueue;

import au.edu.unimelb.cs.culnane.comms.CommunicationLayerMessage;
import au.edu.unimelb.cs.culnane.comms.exceptions.CommunicationLayerMessageException;

public class SocketMessageProcessor implements Runnable {
	private LinkedBlockingQueue<byte[]> processQueue = new LinkedBlockingQueue<byte[]>();
	private volatile boolean shutdown = false;
	private SocketLayer<? extends CommunicationLayerMessage<?>> socketLayer;
	public SocketMessageProcessor(SocketLayer<? extends CommunicationLayerMessage<?>> socketLayer) {
		this.socketLayer=socketLayer;
	}

	@Override
	public void run() {
		try {
			while (!this.shutdown) {
				try {
					this.socketLayer.receive(this.processQueue.take());
				} catch (CommunicationLayerMessageException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void addMsgToProcessQueue(byte[] msg){
		processQueue.add(msg);
	}

}
