package au.edu.unimelb.cs.culnane.comms.layers.securesocket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.edu.unimelb.cs.culnane.comms.CommunicationLayer;
import au.edu.unimelb.cs.culnane.comms.CommunicationLayerMessage;
import au.edu.unimelb.cs.culnane.comms.CommunicationLayerMessageConstructor;
import au.edu.unimelb.cs.culnane.comms.exceptions.CommunicationLayerMessageException;

public class SocketLayer<E extends CommunicationLayerMessage<?>> extends CommunicationLayer<E> {

	private static final Logger logger = LoggerFactory.getLogger(SocketLayer.class);

	private SSLServerSocket serverSocket;
	private SocketListener listener;
	private ExecutorService listenerExec = Executors.newSingleThreadExecutor();

	private ServerSocketFactory sslSocketFactory;

	public SocketLayer(CommunicationLayerMessageConstructor<E> msgConstructor, int listenPort,
			ServerSocketFactory sslSocketFactory) throws IOException {
		super(msgConstructor);
		// this.serverSocket = new SSLServerSocket(listenPort);
		this.sslSocketFactory = sslSocketFactory;
		this.serverSocket = (SSLServerSocket)sslSocketFactory.createServerSocket();
		//serverSocket.setEnabledCipherSuites(new String[] {});
		//serverSocket.setNeedClientAuth(true);
		serverSocket.setUseClientMode(false);
		serverSocket.setReuseAddress(true);

		InetSocketAddress sa = new InetSocketAddress(listenPort);
		serverSocket.bind(sa);

		this.listener = new SocketListener(this.serverSocket, this);
	}

	public void start() {
		listenerExec.execute(listener);
	}

	public void shutdown() {
		this.listener.shutdown();
		this.listenerExec.shutdownNow();
	}

	public void receive(byte[] msg) throws CommunicationLayerMessageException {
		super.messageReceived(super.getMsgConstructor().constructMessage(msg));
		// use endpoint to response
	}

}
