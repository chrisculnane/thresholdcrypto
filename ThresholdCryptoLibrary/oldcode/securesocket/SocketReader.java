package au.edu.unimelb.cs.culnane.comms.layers.securesocket;

import java.io.IOException;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SocketReader implements Runnable {
	private static final Logger logger = LoggerFactory
			.getLogger(SocketReader.class);
	private Socket sock;
	private SocketMessageProcessor messageProcessor;
	private volatile boolean shutdown=false;
	public SocketReader(Socket sock, SocketMessageProcessor messageProcessor) {
		this.sock=sock;
		this.messageProcessor=messageProcessor;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		BoundedBufferedInputStream bbis =null;
		try{
			bbis = new BoundedBufferedInputStream(sock.getInputStream());
			while(!shutdown){
				logger.info("About to wait for readline");
				byte[] msg = bbis.readLine();
				logger.info("Read line from socket");
				if(msg==null){
					logger.info("Closing socket");
					//A null response indicates the stream has been closed
					sock.close();
					break;
				}
				this.messageProcessor.addMsgToProcessQueue(msg);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (StreamBoundExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(bbis!=null){
				try {
					bbis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		

	}

}
