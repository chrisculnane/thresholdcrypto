package au.edu.unimelb.cs.culnane.comms.layers.securesocket;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.edu.unimelb.cs.culnane.comms.CommunicationLayerChannel;
import au.edu.unimelb.cs.culnane.comms.CommunicationLayerMessage;

public class SocketLayerChannel<E extends CommunicationLayerMessage<?>> extends
		CommunicationLayerChannel<E> {
	private static final Logger logger = LoggerFactory
			.getLogger(SocketLayerChannel.class);
	private Thread runner;
	private String id;
	private SSLSocket socket;
	private InetSocketAddress endPoint;
	private BufferedOutputStream bos;
	private LinkedBlockingQueue<byte[]> messageBuffer = new LinkedBlockingQueue<byte[]>();
	private SSLSocketFactory factory;
	public SocketLayerChannel(String id, String host, int port, SSLSocketFactory factory)
			throws UnknownHostException, IOException {
		this.id = id;
		this.factory=factory;
		socket = (SSLSocket) this.factory.createSocket();

        socket.setEnabledCipherSuites(new String[] {});

        //// We want to use ClientAuthentication (mutual authentication)
        //socket.setUseClientMode(true);

       this.endPoint = new InetSocketAddress(host, port);

	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void close() throws IOException {
		logger.info("Closing socket {}",this.id);
		this.socket.close();
		this.runner.interrupt();
	}

	@Override
	public void send(byte[] msg) throws IOException {
		logger.info("Message added to queue");
		messageBuffer.add(msg);

	}

	@Override
	public void send(E msg) throws IOException {
		send(msg.getRawMessage());
	}

	@Override
	public void open() throws IOException {
		this.runner = new Thread(this, this.id + "RunnerThread");
		this.runner.start();
	}

	@Override
	public void run() {
		try {
			while (true) {
				try {
					logger.info("Socket {} trying to open Connection to {}",this.id,this.endPoint);
					this.socket=(SSLSocket) this.factory.createSocket();
					//socket.setEnabledCipherSuites(new String[] {"SSL_RSA_WITH_NULL_SHA" });
					this.socket.connect(this.endPoint);
					logger.info("Socket {} opened Connection to {}",this.id,this.endPoint);
					logger.info("Remote peer:{}",this.socket.getSession().getPeerPrincipal().getName());
					this.bos = new BufferedOutputStream(
							this.socket.getOutputStream());
					while (true) {
						this.bos.write(this.messageBuffer.take());
						
						this.bos.write('\n');
						this.bos.flush();
					}
				} catch (IOException e) {
					logger.info("Socket {} failed to open Connection to {}",this.id,this.endPoint);
					System.out
							.println("Socket connection failed, will retry in 5 seconds");
					Thread.sleep(5000);

				}

			}
		} catch (InterruptedException e) {
			System.out.println("Sending thread interrupted. Shutting down");
		}
	}

}
