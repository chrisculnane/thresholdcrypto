package au.edu.unimelb.cs.culnane.comms.layers.securesocket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SocketListener implements Runnable {
	private static final Logger logger = LoggerFactory
			.getLogger(SocketListener.class);
	private ServerSocket serverSocket;
	private volatile boolean shutdown =false;
	private SocketMessageProcessor messageProcessor;
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	private ExecutorService readers= Executors.newCachedThreadPool();
	public SocketListener(ServerSocket serverSocket,SocketLayer<?> socketLayer) {
		this.serverSocket=serverSocket;		
		this.messageProcessor = new SocketMessageProcessor(socketLayer);
	}

	
	@Override
	public void run() {
		executor.execute(this.messageProcessor);
		// Loop until shutdown
	    while (!this.shutdown) {
	      try {
	        // Get the external socket from the peer and call accept, which blocks until a connection is received
	        Socket socket = this.serverSocket.accept();
	        logger.info("Received a connection from {}",socket);
	        readers.execute(new SocketReader(socket,this.messageProcessor));
	      }
	      catch (IOException e) {
	        //logger.error("IOException whilst accepting connection on External Server Socket", e);
	      }
	    }
	}
	

	  /**
	   * Shuts down the listener.
	   */
	  public void shutdown() {
		  //TODO make more graceful, wait for termination or timeout
		  this.executor.shutdownNow();
		  this.readers.shutdownNow();
		  this.shutdown = true;
	  }

}


